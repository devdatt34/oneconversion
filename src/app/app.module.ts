import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HoverEffectPipe } from './hover-effect.pipe';
import { TodoModule } from './todo/todo.module';
import { from } from 'rxjs';
import { TaskComponent } from './task/task.component';
import { HighlightPipe } from './highlight.pipe';

@NgModule({
  declarations: [AppComponent, HoverEffectPipe, TaskComponent, HighlightPipe],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
