import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-task-completed',
  templateUrl: './task-completed.component.html',
  styleUrls: ['./task-completed.component.scss']
})
export class TaskCompletedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
