import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskCompletedComponent } from './task-completed/task-completed.component';



@NgModule({
  declarations: [TaskCompletedComponent],
  imports: [
    CommonModule
  ]
})
export class TodoModule { }
