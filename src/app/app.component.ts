import { Component } from '@angular/core';
import { Params } from '@angular/router';
import { analyzeFileForInjectables } from '@angular/compiler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'My Project';
  showMsg = true;
  colorName = 'red';
  switchValue = 'three';
  clsName = 'one';

  firstname = '';
  clsName2 = 'two';
  subTitle = 'Its fun to learn the interpolation';
  epNum = 17;
  users = [
    { userid: 1, 'username': 'devid' },
    { userid: 2, 'username': 'doe' },
    { userid: 2, 'username': 'doe' },
    { userid: 2, 'username': 'doe' },
    { userid: 2, 'username': 'doe' }];
  user = {
    userId: 281,
    userName: 'Arc',
    status: 'Active',
    DOB: '07/23/1991',
    salary: 50000.56
  };
  users1 = [{
    firstName: 'Jane Doe',
    city: 'New York',
    cityCode: 'NY'
  },
  {
    firstName: 'Jane Doe',
    city: 'New York',
    cityCode: 'NY'
  },
  {
    firstName: 'Jane Doe',
    city: 'New York',
    cityCode: 'NY'
  }
  ];
  user1 = {
    firstName: 'Jane Doe',
    city: 'New York',
    cityCode: 'NY'
  };
  hrefVal = 'https://google.com';
  updatedLink = 'http://oracle.com';

  colorVal = 'red';

  // tslint:disable-next-line: typedef
  readMe() {
    console.log('button Clicked');
  }
  // tslint:disable-next-line: typedef
  showAlert(id) {
    alert('hello my name is devdatt' + id);
  }
  // tslint:disable-next-line: typedef
  consoleLog() {
    console.log('Hello');
  }
  // tslint:disable-next-line: typedef
  readFirstName() {
    console.log(this.firstname);
  }
}
